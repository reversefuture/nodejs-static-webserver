class Node {
    constructor(val){
        this.val = val
        this.left = null
        this.right = null
    }

   static * preOrder(root){
       let self=this
        if(root){
            yield root.val
            yield* self.preOrder(root.left)
            yield* self.preOrder(root.right)
        }
    }
   static * midOrder(root){
       let self=this
        if(root){
            yield* self.preOrder(root.left)
            yield root.val
            yield* self.preOrder(root.right)
        }
    }
   static * postOrder(root){
       let self=this
        if(root){
            yield* self.preOrder(root.left)
            yield* self.preOrder(root.right)
            yield root.val
        }
    }

}

// test

singer = new Node("Taylor Swift")

genre_country = new Node("Country")
genre_pop = new Node("Pop")

album_fearless = new Node("Fearless")
album_red = new Node("Red")
album_1989 = new Node("1989")
album_reputation = new Node("Reputation")

song_ls = new Node("Love Story")
song_wh = new Node("White Horse")
song_wanegbt = new Node("We Are Never Ever Getting Back Together")
song_ikywt = new Node("I Knew You Were Trouble")
song_sio = new Node("Shake It Off")
song_bb = new Node("Bad Blood")
song_lwymmd = new Node("Look What You Made Me Do")
song_g = new Node("Gorgeous")

singer.left  = genre_country 
singer.right =genre_pop
genre_country.left= album_fearless
genre_country.right =album_red
album_fearless.right = song_wh


console.log(...Node.preOrder(singer))
console.log(...Node.midOrder(singer))
console.log(...Node.postOrder(singer))




// function* ge() { 
//     yield '1';
//     yield '2';
//     yield '3';
//     return '4';
//   }
//   for(let v of ge()){
//     console.log(v);    // 1 2 3 4
//   }