class Node {
    constructor(value) {
        this.value = value
        this.next = null
    }
}

class LinkedList {
    constructor () {
      this.head = new Node('head')
    }
    // 根据value查找节点
    findByValue (item) {
      let currentNode = this.head
      while (currentNode !== null && currentNode.element !== item) {
        currentNode = currentNode.next
      }
      console.log(currentNode)
      return currentNode === null ? -1 : currentNode
    } 
    
    // 根据index查找节点
    findByIndex (index) {
      let currentNode = this.head
      let pos = 0
      while (currentNode !== null && pos !== index) {
        currentNode = currentNode.next
        pos++
      }
      console.log(currentNode)
      return currentNode === null ? -1 : currentNode
    } 
    
    // 指定元素向后插入
    insert (newElement, element) {
      const currentNode = this.findByValue(element)
      if (currentNode === -1) {
        console.log('未找到插入位置')
        return
      }
      const newNode = new Node(newElement)
      newNode.next = currentNode.next
      currentNode.next = newNode
    } 
    
    // 查找前一个
    findPrev (item) {
      let currentNode = this.head
      while (currentNode.next !== null && currentNode.next.element !== item) {
        currentNode = currentNode.next
      }
      if (currentNode.next === null) {
        return -1
      }
      return currentNode
    } 
    
    // 根据值删除
    remove (item) {
      const desNode = this.findByValue(item)
      if (desNode === -1) {
        console.log('未找到元素')
        return
      }
      const prevNode = this.findPrev(item)
      prevNode.next = desNode.next
    } 
    
    // 遍历显示所有节点
    display () {
      let currentNode = this.head
      while (currentNode !== null) {
        console.log(currentNode.element)
        currentNode = currentNode.next
      }
    }
  }

class OrderedLinkedList {
    constructor() {
        //head节点即哨兵，作用就是使所有链表，
        // 包括空链表的头节点不为null,并使对单链表的插入、删除操作不需要区分是否为空表或是否在第一个位置进行，
        // 从而与其他位置的插入、删除操作一致
        //所以反转链表的时候不需要带上head节点
        this.root = new Node('head')
    }
    size() {
        // if (this.root === null) {
        //     return 0
        // }
        let curNode = this.root.next //去掉哨兵
        let sum = 0
        // while (curNode.next !== null) {
        while (curNode !== null) {
            sum++;
            curNode = curNode.next
        }
        return sum
    }
    add(val) { //升序
        if (val === null || val === undefined) {
            console.error('invalid input')
            return
        }
        let curNode = this.root
        let newNode = new Node(val)
        // if (curNode === null) {
        //     this.root = newNode
        //     return
        // }
        while (curNode.next !== null && curNode.next.value < val) {
            curNode = curNode.next
        }
        newNode.next = curNode.next
        curNode.next = newNode

    }
    find(val) {
        if (val === null || val === undefined) {
            console.error('invalid input')
            return
        }
        let curNode = this.root
        // if (curNode === null) {
        //     return null
        // }
        while (curNode !== val && curNode.value !== val) {
            curNode = curNode.next
        }
        return curNode
    }
    remove(val) {
        if (val === null || val === undefined) {
            console.error('invalid input')
            return
        }
        let preNode = this.root
        let curNode = this.root.next
        while (curNode !== null && curNode.value !== val) {
            preNode = curNode
            curNode = curNode.next
        }
        if (curNode == null) {
            console.error('removing value not exist')
        } else {
            preNode.next = curNode.next
        }
    }
    findByIndex(index) { //从1开始
        if (index === null || index === undefined) {
            console.error('invalid input')
            return
        } else if (index > this.size()) {
            console.error('over size')
            return
        }
        let curNode = this.root
        let pos = 1
        while (pos < index) {
            pos++
            curNode = curNode.next
        }
        return curNode
    }
    /**
     * 遍历，提供了head就从head开始，不然就是当前list的head
     * @param {Node} head 
     */
    display(head) {
        let result = []
        let curNode = head || this.root.next //去掉哨兵

        while (curNode !== null) {
            result.push(curNode.value)
            curNode = curNode.next
        }
        console.log(result)
    }
    /**
     * 翻转链表，返回新链表的头节点
     */
    reverse() {
        //第一个节点头结点让其指向null
        let previousNode = null
        let curNode = this.root.next //去掉哨兵
        while (curNode != null) {
            let nextNode = curNode.next //暂存当前链表下一个
            curNode.next = previousNode //翻转当前节点指针
            previousNode = curNode //新链表的头更新
            curNode = nextNode //下一个
        }
        this.root.next = previousNode //最后将反转好的链表接到哨兵头节点下面
        return this.root
    }
    /**
     * 环验证
     * fast 最终
     */
    checkCircle() {
        let slow = this.root
        let fast = slow.next
        while (fast !== null && fast.next !== null) {
            if (fast == slow) {
                return true
            }
            slow = slow.next
            fast = fast.next.next
        }
        return false
    }
    /**
     * 删除倒数第k个节点
     * @param {Number} index 
     */
    removeByIndexFromEnd(index) {
        if (index === null || index === undefined || typeof index !== 'number') {
            console.error('invalid input')
            return
        }
        if(this.checkCircle()){
            console.error('链表存在环，无法从倒数删除')
            return
        }
        let prevNode = this.reverse()
        let curNode = prevNode.next
        let pos =1
        while(curNode!=null && pos< index){
            prevNode = curNode
            curNode = curNode.next
            pos++
        }
        prevNode.next = curNode.next
        this.reverse()
    }
    /**
     *寻找中间节点,寻找3等分点只需要把fast速度变成slow 3倍
     */
    findMiddle(){
        let slow = this.root
        let fast = this.root
        while(fast !== null && fast.next !==null){
            slow=slow.next
            fast = fast.next.next
        }
        return slow
    }
    /**
     * 合并链表
     * @{Node} 带并入的链表
     */
    mergeList(listB){
        let a = this.root.next
        let b = listB.root.next
        let mergedHead = new Node('head');
        let curMergedNode = mergedHead
        while(a !=null && b!=null){
            if(a.value > b.value){
                curMergedNode.next = b
                b = b.next
            }else{
                curMergedNode.next =a
                a = a.next
            }
            curMergedNode = curMergedNode.next
        }
        if(a !=null){
            curMergedNode.next = a
        }else{
            curMergedNode.next = b
        }
        this.root = mergedHead
        return mergedHead
    }
}
module.exports = {
    LinkedList,
    OrderedLinkedList,
    Node
}


// let demoList = new OrderedLinkedList()
// demoList.add(3)
// demoList.add(33)
// demoList.add(36)
// demoList.add(12)
// demoList.add(13)
// demoList.add(8)
// demoList.remove(12)
// demoList.reverse()
// demoList.display()
// console.log(demoList.find(13))
// console.log(demoList.findByIndex(2))

// console.log('-------------check circle------------')
// let circleNode = demoList.find(13)
// let lastNode = demoList.findByIndex(demoList.size())
// console.log(lastNode)
// lastNode.next = circleNode
// console.log(demoList.checkCircle())

// console.log('-------------倒数删除------------')
// demoList.removeByIndexFromEnd(2)
// demoList.display();

// console.log('-------------寻找中间节点------------')
// console.log(demoList.findMiddle())

// console.log('-------------合并------------')
// let demoList2 = new OrderedLinkedList()
// demoList2.add(101)
// demoList2.add(1)
// demoList2.add(123)
// let mDemo =  demoList.mergeList(demoList2)
// console.log(demoList.display(mDemo))