/**
 * 跳跃表的应用 
Skip list(跳表）是一种可以代替平衡树的数据结构，默认是按照Key值升序的。Skip list让已排序的数据分布在多层链表中，以0-1随机数决定一个数据的向上攀升与否，通过“空间来换取时间”的一个算法，在每个节点中增加了向前的指针，在插入、删除、查找时可以忽略一些不可能涉及到的结点，从而提高了效率。 
在Java的API中已经有了实现：分别是 
ConcurrentSkipListMap(在功能上对应HashTable、HashMap、TreeMap) ； 
ConcurrentSkipListSet(在功能上对应HashSet). 
确切来说，SkipList更像Java中的TreeMap，TreeMap基于红黑树（一种自平衡二叉查找树）实现的，时间复杂度平均能达到O(log n)。 
HashMap是基于散列表实现的，时间复杂度平均能达到O(1)。ConcurrentSkipListMap是基于跳表实现的，时间复杂度平均能达到O(log n)。 
Skip list的性质 
(1) 由很多层结构组成，level是通过一定的概率随机产生的。 
(2) 每一层都是一个有序的链表，默认是升序 
(3) 最底层(Level 1)的链表包含所有元素。 
(4) 如果一个元素出现在Level i 的链表中，则它在Level i 之下的链表也都会出现。 
(5) 每个节点包含两个指针，一个指向同一链表中的下一个元素，一个指向下面一层的元素。
 **   查找的时间复杂度就为 3∗log2n=logn, 插入删除也是logn
    跳表的空间复杂度为 O(n)
    第0层包含所有元素

存储结构1：假设有k层
    -Header:{ next: [
        node1: {
            next: [
                node: {next :... ,val: xx}
            node2, ...nodeK], val: xx
        }
    node2, ...nodeK], val:xx};
    -对于表的第i层，i越大，节点越少，从header开始到第0层的路径越长，嵌套越深；
    -这里的层已经不是线性的，而是嵌套
    -假设遍历到第i层，当前节点cur重置为Header, 找到当前层i大于key的最后节点后开始遍历下一层i-1
    
 
存储结构2：
   -每个node有up,down,left,right 4个指针，通过这4个指针遍历，基本顺序为right,right.... lastRight > down,right,right...lastRight > down...
 */

class SkipNode{
    constructor(MAX_LEVEL, val) {
        this.next = new Array(MAX_LEVEL) // 元素是SkipNode
        this.val = val;
    }
    /**
     * 误差范围设为2的-50次方
     * @param {*} val 
     */
    equal(val){
        return Math.abs(this.val - val) < Number.EPSILON * Math.pow(2, 2);
    }
    compareTo(val){
        this.val - val
    }
}

class Node2{
    constructor(key, val) {
        this.key=key
        this.val = val
        this.up=null
        this.down=null
        this.left=null
        this.right=null
    }
}

class SkipList{
    constructor(){
        this.MAX_LEVEL=32 //32层，理论上对2^32-1个元素的查询最优
        this.level = 0 //当前层
        this.Headers = new SkipNode(this.MAX_LEVEL, null)
        // this.random = new RandomSource() //随机发生器
    }
    
    /**
     * 检查是否包含val节点
     * @param {*} val 
     */
    contains(val){
        /*
         * 从顶层开始查找当前层的链表中是否包含节点node，如果包含node节点，直接返回true；否则在下一层中查找是否包含node节点。
         * 如果最底层的链表也不包含node节点，则放回false。
         */   
        let cur = this.Headers
        for(let i=this.level; i>=0; i--){
            while(cur.next !=null && cur.next[i].val< val){
                cur = cur.next[i]
            }
            if(cur.next[i].val == val){
                return true
            }
        }
        return false
    }
   
    /**
     * 插入
     * @param {*} val 
     */
    insert(val){
        let cur = this.Headers
        let predecessors = new Array(this.MAX_LEVEL) //save pre nodes in each level
        /*
         * 找出每层中待插入节点的前继节点
         */
        for(let i=this.level; i>=0;i--){
            cur=this.Headers
            while(cur.next[i]!=null && cur.next[i].val< val){
                console.log(cur.next)
                cur=cur.next
            }
            predecessors[i] = cur //cur此时为最后第一个 <val的节点
        }
        cur=cur.next[0] //取得第0层对应位置，用来添加的时候判断这个节点是否存在
        let nextLevel = this.randomLevel() //随机生成插入级数
         /*
         * 如果带插入节点位置是空的或者与node节点值不同 将新节点插入到跳表中
         */
        if(cur==null || cur.val !== val){
            if(nextLevel >this.level){ //新增一层链表
                predecessors[nextLevel] =this.Headers
                this.level =nextLevel
            }
            let node = new SkipNode(this.MAX_LEVEL, val)
            for(let i=this.level;i>=0;i--){//从level层开始，逐级向下插入新节点
                node.next[i] = predecessors[i].next[i]
                predecessors[i].next[i]=  node
            }
        }
    }
    /**
     * 删除
     * @param {*} val 
     */
    delete(val){
        let cur = this.Headers
        let predecessors = new Array(this.MAX_LEVEL)
        /*
         * 找出每层中待删除节点的前继节点
         */
        for(let i=this.level; i>=0;i--){
            cur=this.Headers
            while(cur.next[i]!=null && cur.next[i].val< val){
                cur=cur.next
            }
            predecessors[i] = cur //cur此时为最后第一个 <val的节点
        }
        cur=cur.next[0]
        //表中不含此节点
        if(cur.val !== val){
            return
        }
        for(let i=this.level; i>=0;i--){
            if(predecessors[i].next[i].val !== val){
                continue
            }
            predecessors[i].next[i] = cur.next[i] //删
        }
        //如果删除元素val后level层元素数目为0，层数减少一层
        while(this.level > 0 && this.Headers.next[this.level] ==null){
            this.level--
        }
    }
    /**
     * 输出跳表中的元素
     */
     toString() {
        let sb = []
        let cur = this.Headers.next[0];
        while (cur.next[0] != null) { //第0层包含所有元素
            sb.push(cur.val);
            cur = cur.next[0];
        }
        sb.push(cur.val);
        return sb.toString();
    }
    /**
     * 利用随机数发生器来决定是否新增一层
     * 
     * @return
     */
   randomLevel() {
        let ins = Math.random(0,1)*10;
        let nextLevel = this.level;
        if (ins > Math.E && this.level < this.MAX_LEVEL) {
            nextLevel++;
        }
        return nextLevel;
    }
}

//test
l = new SkipList()
for (let i = 0; i < 10; i++) {
    let ele = Math.random(0,1)* 100;
    l.insert(ele)
}
l.toString()
l.insert(66)
l.toString()
l.insert(133)
l.toString()
l.delete(66)
l.toString