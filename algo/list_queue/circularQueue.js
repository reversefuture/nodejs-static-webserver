const {Node} = require('./linkedList')

/**
 * 在tail增在head删
 *  head(node1)-> node2 -> node3 ... -> tail(noden) --empty space-> head
 * 增
 * 初始空： head(tail) -> head(tail)
 *  head(node1)-> node2 -> node3 ... -> -> noden -> tail(noden+1) -> head
 * 删
 *  head(node2) -> node3 ... -> tail(noden) -> head
 */
class CirclularQueue{
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }
    /**
     * 在tail尾部添加
     */
    enqueue(val){
        let node = new Node(val)
        if(this.head == null){ //head为第一个，如果为Null队列为空
            this.head = node
            this.head.next = this.head
            this.tail = this.head
        }else{
            let hasOnlyOneNode = this.head === this.tail
            this.tail.next = node
            this.tail.next.next = this.head
            this.tail = this.tail.next
            if(hasOnlyOneNode){
                this.head.next = this.tail
            }
        }
        this.size++
    }
    /**
     * 在head头部删除
     */
    dequeue(){
        let result = null;
        if(this.head ==null){
            return null
        }else if(this.head === this.tail){
            result = this.head
            this.head = null
        }      
        else{
            result = this.head
            this.head = this.head.next
            this.tail.next = this.head
        }
        this.size--
        return result
    }
    display(){
        let result = []
        let curNode = this.head
        while(curNode!=null && curNode !== this.tail){
            result.push(curNode.value)
            curNode = curNode.next
        }
        if(this.tail){
            result.push(this.tail.value)
        }
        console.log(JSON.stringify(result))
    }
}

//test
let demoQueue = new CirclularQueue()
demoQueue.enqueue(1)
demoQueue.enqueue(4)
demoQueue.enqueue(51)
//51 tail -> 4 -> 1 (head)
//应该为 1(head) -> 4 -> 51(tail)
demoQueue.display()

console.log(demoQueue.dequeue())
demoQueue.display()