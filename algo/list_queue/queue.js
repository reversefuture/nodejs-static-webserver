const {LinkedList,Node} = require('./linkedList')

/**
 *  head(node1)-> node2 -> node3 ... -> tail(noden)
 * 增
 *  head(node1)-> node2 -> node3 ... -> -> noden -> tail(noden+1)
 * 删
 *  head(node2) -> node3 ... -> tail(noden)
 */
class Queue{
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }
    /**
     * 在tail尾部添加
     */
    enqueue(val){
        let node = new Node(val)
        if(this.head == null){ //font为第一个，如果为Null队列为空
            this.head = node
            this.tail = node
        }else{
            this.tail.next = node
            this.tail = node
        }
        this.size++
    }
    /**
     * 在head头部删除
     */
    dequeue(){
        let result = null;
        if(this.head ==null){
            return null
        }else{
            result = this.head
            this.head = this.head.next
            this.size--
        }
        return result
    }
    display(){
        let result = []
        let curNode = this.head
        while(curNode !=null){
            result.push(curNode.value)
            curNode = curNode.next
        }
        console.log(JSON.stringify(result))
    }
}
module.exports = {
    Queue
}


//test
let demoQueue = new Queue()
demoQueue.enqueue(1)
demoQueue.enqueue(4)
demoQueue.enqueue(51)
//51 tail -> 4 -> 1 (head)
//应该为 1(head) -> 4 -> 51(tail)
demoQueue.display()

console.log(demoQueue.dequeue())
demoQueue.display()