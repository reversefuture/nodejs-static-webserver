const LinkedList = require('./linkedList')

console.log(LinkedList)
class SampleBrowser{
    constructor(){
        this.normalStack = []
        this.backStack = []
    }
    /**
     * 正常浏览
     * @param {string} name 
     */
    browse(name){
        this.backStack = []
        this.normalStack.push(name)
        this.displayAllStack()
    }
    back(){
        let name = this.normalStack.pop()
        if(name !=undefined){
            this.backStack.push(name)
        }else{
            console.warn('history is empty, cannot go back')
        }
    }
    front(){
        let name = this.backStack.pop()
        if(name !=undefined){
            this.normalStack.push(name)
        }else{
            console.warn('back history is empty, cannot go forward')
        }
    }

    displayAllStack(){
        console.log('-------------normalStack----------------------')
        console.log(this.normalStack)
        console.log('-------------backStack----------------------')
        console.log(this.backStack)
    }
}

// Test
const browser = new SampleBrowser()
browser.browse('www.google.com')
browser.browse('www.baidu.com')
browser.browse('www.github.com')
// 后退
browser.back()
browser.back()
browser.displayAllStack()
browser.front()
browser.browse('www.new.com')
browser.displayAllStack()