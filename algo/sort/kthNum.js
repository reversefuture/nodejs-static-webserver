/**
 *https://www.cnblogs.com/kyoner/p/10465633.html
 
 * 第k大的数. 按照降序排的第k个，1开始
 * 快排， 取数组最右边的数字作为选择的数字，从数组的第一个数字开始与选择数字进行比较，将小于选择数字放入数组左边，
 * 当挑选出所有小于选择数子的数时，将选择数字放入中间。则数组剩余的右边部分为大于等于选择数字的所有数
 * 每趟排序完返回中间点Index, index > k，说明k大的数在index左边，下一趟在start -- index之间快排
 * @param {array} arr 
 * @param {number} k  
 */
function kthNum(arr, k) {
    const len = arr.length;
    if (k > len) {
      return -1;
    }
    let p = partition3(arr, 0, len - 1);
    while (p + 1 !== k) { //k是从1开始的
      if (p + 1 > k) {
        p = partition3(arr, 0, p - 1); 
      } else {
        p = partition3(arr, p + 1, len - 1);
      }
    }
    console.log(arr)
    // return arr[p];
    return arr[arr.length - p];
  }
  
  function partition(arr, start, end) {
    let small = start;
    let pivot = arr[end];
    for (let j = start; j < end; j++) {
      if (arr[j] > pivot) {
        swap(arr, small, j);
        small += 1;
      }
    }
    swap(arr, small, end);
    return small;
  }
  
  function partition2(arr, start, end){
      if(arr==null || start <0 || end >arr.length-1){
        console.error("invalid input")
      }
      //small表示小于选择数字的下标，index表示遍历数组的下标
      //data[1]与选择数字比较，若小于选择数字，则small向后移，index向后移。继续比较data[2]和选择数字。
      //data[2]与被选择数字比较，不小于选择数字，则index后移，small不移动(记录这个逆序数字）。
      //当比较到第n个数字data[n]时，小于选择数字，将data[n]与逆序数字data[small]交换
      let small = start - 1
      for(let i = start; i < end; i++){
          if(arr[i] < arr[end]){
              if(small != i){
                  swap(arr, small, i)
              }
              small++
          }
      }
      //最后由于small之前取start-1,+1，再交换一次，将被选择数字放在数组中间，小于部分在右侧，大于等于部分在左侧。
      small++
      swap(arr, small, end)
      return small
  }

  /**直接把快排的代码搬过来 */
  function partition3(arr, low, high) {
    let l = low ? low : 0;
    let h = high ? high : arr.length - 1;
    if (l >= h) {
        return
    }
    let pivot = arr[l]; //基准值，可以取最前或最后

    while (l < h) {
        while (l < h && arr[h] >= pivot) { //找到右边第一个比pivot小的
            h--;
        }
        arr[l] = arr[h] // 将较小的值放在左边如果没有找到比基准值小的数就是将自己赋值给自己（l 等于 h）
        while (l < h && arr[l] <= pivot) { //找到左边第一个比pivot大的
            l++;
        }
        arr[h] = arr[l] // 将较大的值放在右边如果没有比基准值大的数就是将自己赋值给自己
    }
    arr[l] = pivot // 将基准值放至中央位置完成一次循环（这时候 j 等于 i ）
    return l
}

  function swap(arr, i, j) {
    if (i === j) return;
    let tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
  }
  
//test
let demoArr = [3,4,7,95,13,653,324,4,7,1]
let knum = kthNum(demoArr, 4)
console.log(knum)