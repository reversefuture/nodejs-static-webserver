/**
 *参考 https://www.cnblogs.com/onepixel/articles/7674659.html 
 */
function checkTime(fn) {
    return function (arr) {
        let start = Date.now();
        let res = fn(arr);
        let end = Date.now();
        console.log(`>>${fn.name} use Time:` + (end - start))
        return res;
    }
}

function swap(arr, i, j) {
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
    return arr
}

function bubbleSort(arr) {
    let times = 0;
    for (let i = 0, len = arr.length - 1; i < len; i++) { // => i=arr.length-1; i>=0;i--
        for (let j = 0; j < len - i; j++) { //=> j=0;j<i;j++
            if (arr[j] > arr[j + 1]) {
                // [arr[0], arr[i]] = [arr[i], arr[0]] //交换数组元素不要用这个
                swap(arr, j, j + 1)
                times++;
            }
        }
    }
    // console.log(times);
    return arr;
}

function bubbleSort1(arr) {
    let times = 0;
    let isSwapped = false;
    for (let i in arr) {
        for (let j = 0; j < arr.length - 1 - i; j++) {
            if (arr[j] > arr[j + 1]) {
                [arr[j + 1], arr[j]] = [arr[j], arr[j + 1]];
                times++;
                isSwapped = true;
            }
        }
        if (!isSwapped) {
            break;
        }
    }
    // console.log(times);
    return arr;
}

function bubbleSort2(arr) {
    let times = 0;
    for (let i in arr) {
        let lastSorted = 0;
        for (let j = lastSorted; j < arr.length - 1 - i; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(arr, j, i + 1)
                times++;
                lastSorted = j;
            }
        }
    }
    // console.log(times)
    return arr;
}
/**
 * 插入排序和冒泡排序一样，都是每次交换响铃元素消除一个逆序对
 * best T=O(N)
 * worst T=O(N^2)
 * @param {*} arr 
 */
function insertion(arr) {
    let left = 1, right = arr.length - 1
    for (let i = left; i <= right; i++) {
        let preIndex = i - 1
        let current = arr[i] //摸下一张牌
        while (preIndex >= 0 && arr[preIndex] > current) {
            arr[preIndex + 1] = arr[preIndex]
            preIndex--
        }
        arr[preIndex + 1] = current //preIndex在while结束的时候并没有-1
    }
    return arr
}

function insertion2(a) {
    for (let i = 1; i < a.length; i++) {
        temp = a[i] //摸下一张牌
        for (let j = i; j > 0 && a[j - 1] > temp; j--) { //把i 左边比temp大的都往右边挪
            a[j] = a[j - 1] //移出空位
        }
        a[i] = temp //新牌落位
    }
    return a
}


/**
 * 希尔排序，插入排序改进，优先比较距离较远的元素，又叫缩小增量排序，每次交换消掉多个逆序对
 * 定义增量序列: Dm > Dm-1 > ... D1=1
 * 对每个Dk进行 Dk间隔 的插入排序
 * Dk-1排序不影响Dk排序结果
 * 原始增量序列: Dm=N/2, Dk = Dk+1 /2, T=OT(N2),因为增量序列不互质。 *OT为seta，既是上界又是下界
 * Hibbard增量： Dk = 2^k -1, worst: T= O(N^3/2)
 * Sedgewick: 9*4^i - 9*2^i + 1, or: 4^i - 3*2*i + 1
 * Tavg = O(N7/6)  Tworst = O(N4/3)
 */
function shell(arr) {
    var len = arr.length;
    for (var gap = Math.floor(len / 2); gap > 0; gap = Math.floor(gap / 2)) {
        // 注意：这里和动图演示的不一样，动图是分组执行，实际操作是多个分组交替执行
        for (var i = gap; i < len; i++) { // 插入排序
            var j = i;
            var current = arr[i];
            while (j - gap >= 0 && current < arr[j - gap]) {
                arr[j] = arr[j - gap];
                j = j - gap;
            }
            arr[j] = current;
        }
    }
    return arr
}

/**
 * 选择排序
 * 无论如何 T=O(N^2)
 * 关键在于每一趟查找i - N-1最小元需要花费O(N)
 * @param {*} arr 
 */
function selectSort(arr) {
    let times = 0;
    for (let i in arr) {
        for (let j = 0; j < arr.length - 1 - i; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(arr, j, i + 1)
                times++;
            }
        }
    }
    return arr;
}


/**
 * 堆排序，对选择排序的改进，把查找每一趟最小的时间优化为O(logN)
 * 将初始待排序关键字序列(R1,R2….Rn)构建成大顶堆，此堆为初始的无序区；
将堆顶元素R[1]与最后一个元素R[n]交换，此时得到新的无序区(R1,R2,……Rn-1)和新的有序区(Rn),且满足R[1,2…n-1]<=R[n]；
由于交换后新的堆顶R[1]可能违反堆的性质，因此需要对当前无序区(R1,R2,……Rn-1)调整为新堆，然后再次将R[1]与无序区最后一个元素交换，得到新的无序区(R1,R2….Rn-2)和新的有序区(Rn-1,Rn)。
不断重复此过程直到有序区的元素个数为n-1，则整个排序过程完成。
         */
function heapSort(arr) {
    var len = arr.length
    for (var i = Math.floor(len / 2); i >= 0; i--) { // 建立最大堆，[0]不存放哨兵而是元素
        heapify(arr, i, arr.length);
    }
    for (var i = arr.length - 1; i > 0; i--) {
        arr = swap(arr, 0, i) //把最大的放在最后
        heapify(arr, 0, i); ///删除最大堆顶,堆调整排除最后一个元素
    }
    return arr;
}
//调整的时候原则是指考虑当前根子树，子树的子树是通过循环最低子树开始调用heapSort已经调整好了
function heapify(arr, i, len) { //调整最大堆
    let parent = i;
    let child = i * 2 + 1;
    while (child < len) {
        if (child + 1 < len && arr[child + 1] > arr[child]) { // Child指向左右子结点的较大者
            child++;
        }
        if (arr[parent] > arr[child]) { //向下调整如果到当前子树平衡了就返回
            return
        } else {/* 下滤X */
            swap(arr, parent, child) //其实不用没个子树都交换，只用保证堆顶的i是最大的就行了
            parent = child;
            child = parent * 2 + 1
        }
    }
}

function heapify2(a, i, len) { //调整最大堆
    let parent = i;
    let child = i * 2 + 1;
    let max = a[parent] //取出根节点存放值
    while (child < len) {
        if (child + 1 < len && arr[child + 1] > arr[child]) { // Child指向左右子结点的较大者
            child++;
        }
        if (arr[parent] > arr[child]) { //向下调整如果到当前子树平衡了就返回。再下层不考虑。通过循环调用heapify并不断缩小len来覆盖下层
            return
        } else {/* 下滤X */
            a[parent] = a[child] //把parent设置为最大，到while最后parent一定指向从i - len的最大值
            parent = child
            child = parent * 2 + 1
        }
    }
    a[parent] = max
}




/**
 * 归并排序: 分而治之
 * 把长度为n的输入序列分成两个长度为n/2的子序列；
    对这两个子序列分别采用归并排序；
    将两个排序好的子序列合并成一个最终的排序序列。
 */
function mergeSort(arr) {
    let len = arr.length
    if (len < 2) {
        return arr
    }
    let middle = Math.floor(len / 2)
    let left = arr.slice(0, middle)
    let right = arr.slice(middle)
    return merge(mergeSort(left), mergeSort(right))

    function merge(left, right) {
        let result = []
         //将左右两个子序列按顺序导入第三个序列
        while (left.length > 0 && right.length > 0) {
            if (left[0] < right[0]) {
                result.push(left.shift())
            } else {
                result.push(right.shift())
            }
        }
        if (left.length) {
            result = result.concat(left)
        } else {
            result = result.concat(right)
        }
        return result
    }
}

/**
 * 快速排序：也是分治算法，不过每次都是在原数组直接操作，而不是像归并排序那样在新数组上拍好后再合并 
 * 快速排序使用分治法来把一个串（list）分为两个子串（sub-lists）。具体算法描述如下：
    从数列中挑出一个元素，称为 “基准”（pivot）；
    重新排序数列，所有元素比基准值小的摆放在基准前面，所有元素比基准值大的摆在基准的后面（相同的数可以到任一边）。在这个分区退出之后，该基准就处于数列的中间位置。这个称为分区（partition）操作；
    递归地（recursive）把小于基准值元素的子数列和大于基准值元素的子数列排序。
    快排最好的情况是每次选取的主元都处于中间。 理想的T(n) = O(log(n)*n),和二分法一样。
    选主元： 常用的是取头，中，尾的中位数。它之所以快是因为一趟跑完后相对小和相对大的元素都放在了大致的位置。遇到相等的情况应该交换。
 */
function quickSort(arr, low, high) {
    let l = low ? low : 0;
    let h = high ? high : arr.length - 1;
    if (l >= h) {
        return
    }
    let pivot = arr[l]; //基准值，可以取最前或最后

    while (l < h) {
        while (l < h && arr[h] >= pivot) { //找到右边第一个比pivot小的
            h--;
        }
        arr[l] = arr[h] // 将较小的值放在左边如果没有找到比基准值小的数就是将自己赋值给自己（l 等于 h）
        while (l < h && arr[l] <= pivot) { //找到左边第一个比pivot大的
            l++;
        }
        arr[h] = arr[l] // 将较大的值放在右边如果没有比基准值大的数就是将自己赋值给自己
    }
    arr[l] = pivot // 将基准值放至中央位置完成一次循环（这时候 j 等于 i ）

    if (l - 1 > low) quickSort(arr, low, l - 1) // 如果上面的查找找到了新的l, h，那么说明有无序对,将左边的无序数组重复上面的操作
    if (h + 1 < high) quickSort(arr, l + 1, high)
    return arr
}
/**
 * 快排升级，当> cutOff用快排，否则用插入排序
 * @param {*} globalArr 
 * @param {*} start 
 * @param {*} end 
 * @param {*} cutOff 阈值
 */
function quickSort2(globalArr, start = 0, end = globalArr.length - 1, cutOff = 2) {
    if (start >= end) {
        return
    }
    if (cutOff < end - start) {
        let pivot = median3(globalArr, start, end) //此时基准在i--j的最右边
        let low = start, high = end - 1
        while (1) {
            while (globalArr[low] < pivot) low++;
            while (globalArr[high] > pivot) high--;
            if (low < high) swap(globalArr, low, high);
            else break;
        }
        globalArr = swap(globalArr, low, end); /* 将藏在end的基准换到正确的位置low*/
        quickSort2(globalArr, start, low - 1); /**大小顺序:  start < low（pivot） <end*/
        quickSort2(globalArr, low + 1, end);
    } else {
        let temp = insertion(globalArr.slice(start, end + 1))/* 元素太少，用简单排序 */
        globalArr.splice(start, end - start + 1, ...temp);
    }



    //选主元，交换使得a[left]<a[center]<a[right], 并把主元放到right的位置
    function median3(arr, left, right) {
        let center = Math.floor((left + right) / 2)
        if (arr[left] > arr[center]) {
            arr = swap(arr, left, center)
        }
        if (arr[left] > arr[right]) {
            arr = swap(arr, left, right)
        }
        if (arr[center] > arr[right]) {
            arr = swap(arr, center, right)
        }
        swap(arr, center, right); /* 将基准Pivot藏到右边*/
        /* 只需要考虑A[Left] … A[Right-1] */
        return arr[right];
    }
}


let testArr = [1, 3, 11, 4, 52, 2, 6, 7, 8, 32, 55, 543, 23, 64, 9, 41, 66]

// console.log(checkTime(bubbleSort)(testArr.slice(0)))
// console.log(checkTime(bubbleSort1)(testArr.slice(0)))
// console.log(checkTime(bubbleSort2)(testArr.slice(0)))
// console.log(checkTime(insertion)(testArr.slice(0)))
// console.log(checkTime(shell)(testArr.slice(0)))
// console.log(checkTime(mergeSort)(testArr.slice(0)))
var globalArr = testArr.slice(0)
console.log(checkTime(quickSort2)(globalArr))
console.log(globalArr)
// console.log(checkTime(heapSort)(testArr.slice(0)))
// console.log(checkTime(heapSort)(testArr.slice(0)))