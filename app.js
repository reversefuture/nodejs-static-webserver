const path = require('path');
const fs = require('fs');
const StaticServer = require('./static-server');
var proxy = require('http-proxy-middleware');
var express = require('express');
var Busboy = require('busboy');

var app = express();


var options = {
    target: 'https://api.zyctd.cc/api', // 目标服务器 host
    changeOrigin: true, // 默认false，是否需要改变原始主机头为目标URL
    ws: true, // 是否代理websockets
    pathRewrite: {
        '^/_api': '/', // remove base path
    },
    router: {
        // 如果请求主机 == 'dev.localhost:3000',
        // 重写目标服务器 'http://www.example.org' 为 'http://localhost:8000'
        // 'dev.localhost:3000' : 'http://localhost:8000'
    },
    "secure": false
};
var zhuantiOptions = {
    target: 'http://zhuanti.zyctd.tech/', // 目标服务器 host
    changeOrigin: true, // 默认false，是否需要改变原始主机头为目标URL
    ws: true, // 是否代理websockets
    pathRewrite: {
        
    },
    "secure": false
};
var localoptions = {
    target: 'https://api.zyctd.cc/api', // 目标服务器 host
    changeOrigin: true, // 默认false，是否需要改变原始主机头为目标URL
    ws: true, // 是否代理websockets
    pathRewrite: {
        '^/_local': '/', // remove base path
    },
    "secure": false
};
var testServerOptions = {
    target: 'http://android.zyctd.tech', // 目标服务器 host
    changeOrigin: true, // 默认false，是否需要改变原始主机头为目标URL
    ws: true, // 是否代理websockets
    pathRewrite: {
        '^/android': '/', // remove base path
    },
    "secure": false
};


var exampleProxy = proxy(options); //开启代理功能，并加载配置
var zhuantiProxy = proxy(zhuantiOptions); //开启代理功能，并加载配置
var localProxy = proxy(localoptions); //开启代理功能，并加载配置
var testServerProxy = proxy(testServerOptions); //开启代理功能，并加载配置

app.use(function (req, res,next) {
    console.log(req.method + '  ' + req.url);
    next();
})

// (new StaticServer()).start();
app.use('/static', express.static(path.join(__dirname, 'static')))
app.get('/localApi/test', function(req, res) {
    res.send('Hello World');
});

// 文件上传至服务器后 路径path变为：public\files\images\W-jy9YsxsPjNpQHslzGvdXBk.jpg
// 由于在app.js中设置过public为默认路径，所以整理地址时需要去掉public，并且把‘\’变成‘/’
//上传文件必须是post方式并且需要指定上传的路径
app.post('/upload', function(req, res) {
    var _filename='';
    //通过请求头信息创建busboy对象
    let busboy = new Busboy({
        headers: req.headers
    });

    //将流链接到busboy对象
    req.pipe(busboy);

    //监听file事件获取文件(字段名，文件，文件名，传输编码，mime类型)
    busboy.on('file', function(filedname, file, filename, encoding, mimetype) {
        //创建一个可写流
        let writeStream = fs.createWriteStream('./static/' + filename);
        _filename=filename;
        //监听data事件，接收传过来的文件，如果文件过大，此事件将会执行多次，此方法必须写在file方法里
        file.on('data', function(data) {
            writeStream.write(data);
        })

        //监听end事件，文件数据接收完毕，关闭这个可写流
        file.on('end', function(data) {
            writeStream.end();
        });
    });

    //监听finish完成事件,完成后重定向到百度首页
    busboy.on('finish', function() {
        // res.writeHead(303, {
        //     Connection: 'close',
        //     Location: '/static'
        // });
        res.send({
            msg: 'success',
            imgSrc: '/static/' + _filename
        });
    });
});

app.post('/api/post', function(req, res) {
  console.log(req.body);
  res.json(req.body);
});



// app.use('/api', proxy({target: 'https://api.zyctd.cc', changeOrigin: true})); //代理/api/xxx到 dest/api/xxx
// app.use('/_local', localProxy); //使用了pathRewrite就不要在app.use里面加url, 还不能设置多个proxy
// app.use('/_api', exampleProxy);
// app.use('/android', testServerProxy);
// app.use('/', zhuantiProxy);

// app.use( exampleProxy);

var server = app.listen(3002, function() {

    var host = server.address().address
    var port = server.address().port

    console.log("应用实例，访问地址为 http://%s:%s", host, port)

})