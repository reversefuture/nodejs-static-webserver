console.log(self)

self.addEventListener('message', function (e) {
  console.log(e)
  var data = e.data;
  switch (data.type) {
    case 0:
      importScripts('xlsx.core.min.js');
      self.postMessage({
        type: 100,
        msg: 'worker is ready',
      });
      break;  
    case 1:
      console.log('main message', data.msg)
      parseExcel(data.data)
      self.postMessage({
        type: 101,
        msg: 'worker started',
      });
      break;
    case -1:
        console.log('main message', data.msg)
        self.close(); // Terminates the worker.
        break;
    default:
      self.postMessage({
        type: 102,
        msg: 'Unknown main command: ' + data.msg,
      });
  };
}, false);

function parseExcel(files) {
  var fileReader = new FileReader();
  fileReader.onload = function (ev) {
    var data = ev.target.result,
    workbook = XLSX.read(data, {
      type: 'binary'
    }), // 以二进制流方式读取得到整份excel表格对象
    readData = []; // 存储获取到的数据
    // 表格的表格范围，可用于判断表头是否数量是否正确
    var fromTo = '';
    // 遍历每张表读取
    for (var sheet in workbook.Sheets) {
      if (workbook.Sheets.hasOwnProperty(sheet)) {
        fromTo = workbook.Sheets[sheet]['!ref'];
        console.log(fromTo);
        readData = readData.concat(XLSX.utils.sheet_to_json(workbook.Sheets[sheet]));
        // break; // 如果只取第一张表，就取消注释这行
      }
    }
  
    
    self.postMessage({
      type: 110,
      msg: 'parse done',
      data: readData
    });
    // $('.data').text(JSON.stringify(readData))
  };
  // 以二进制方式打开文件
  fileReader.readAsBinaryString(files[0]);
}
