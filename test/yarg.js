#!/usr/bin/env node
let argv = require('yargs').argv;
console.log('hello ',argv.name);

//执行： yarg.js --name=zxmf 或者 yarg.js --name zxmf
//process.argv: [ '/usr/local/bin/node', '/usr/local/bin/yarg', '--name=zxmf' ]
//Argv:     {name: 'zxmf',}

/**
 * argv对象有一个下划线属性，可以获取非连词线开头的参数
let argv = require('yargs').argv
console.log('hello ',argv.n);
console.log(argv._);

hello A -n zxmf B C
hello zxmf ['A','B','C']

作者：_小山楂
链接：https://www.jianshu.com/p/7851001dd93b
来源：简书
简书著作权归作者所有，任何形式的转载都请联系作者获得授权并注明出处。
 */